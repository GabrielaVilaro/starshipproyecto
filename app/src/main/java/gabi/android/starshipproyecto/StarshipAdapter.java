package gabi.android.starshipproyecto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gabi.android.starshipproyecto.rest.StarshipApi;


public class StarshipAdapter  extends RecyclerView.Adapter<StarshipAdapter.ViewHolder> {

    private Context context;
    private List<StarshipApi> starships;
    private OnClickListener onClickListener;

    public StarshipAdapter(Context context, List<StarshipApi> starships, OnClickListener onClickListener) {
        this.context = context;
        this.starships = starships;
        this.onClickListener = onClickListener;
    }

    @Override
    public StarshipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_starship, parent,false);
        return new ViewHolder(view, onClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewName.setText(starships.get(position).getName());
        holder.textViewModelo.setText(starships.get(position).getModel());
    }

    @Override
    public int getItemCount() {
        if(starships != null){
            return starships.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textViewName;
        TextView textViewModelo;
        OnClickListener listener;

        public ViewHolder(View itemView, OnClickListener listener) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewModelo = itemView.findViewById(R.id.textViewModelo);
            this.listener = listener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.click(getAdapterPosition());
        }

    }

    public interface OnClickListener{

        void click(int position);

    }

}
