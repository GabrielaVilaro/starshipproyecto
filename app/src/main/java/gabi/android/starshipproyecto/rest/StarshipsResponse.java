package gabi.android.starshipproyecto.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StarshipsResponse {

    @SerializedName("results")
    private List<StarshipApi> starships;

    public List<StarshipApi> getStarships() {return starships;}

}
