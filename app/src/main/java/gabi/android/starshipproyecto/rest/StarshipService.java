package gabi.android.starshipproyecto.rest;

import retrofit2.Call;
import retrofit2.http.GET;

public interface StarshipService {

    @GET("starships")
    Call<StarshipsResponse> getStarships();


}
