package gabi.android.starshipproyecto.rest;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class StarshipApi implements Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("model")
    private String model ;

    @SerializedName("starship_class")
    private  String clase;

    @SerializedName("manufacturer")
    private  String manufacturer;

    @SerializedName("MGLT")
    private String speed;

    @SerializedName("length")
    private String length ;

    @SerializedName("crew")
    private String crew;

    @SerializedName("passengers")
    private  String passenger;

    @SerializedName("cargo_capacity")
    private  String cargo;

    public StarshipApi(String name, String model, String clase, String manufacturer, String speed, String length, String passenger, String cargo, String crew) {
        this.name = name;
        this.model = model;
        this.clase = clase;
        this.manufacturer = manufacturer;
        this.speed = speed;
        this.length = length;
        this.passenger = passenger;
        this.cargo = cargo;
        this.crew = crew;
    }

    public static final Creator<StarshipApi> CREATOR = new Creator<StarshipApi>() {
        @Override
        public StarshipApi createFromParcel(Parcel source) {
            return new StarshipApi(source);
        }

        @Override
        public StarshipApi[] newArray(int size) {
            return new StarshipApi[0];
        }
    };

    public StarshipApi(Parcel source) {

        name = source.readString();
        model = source.readString();
        clase = source.readString();
        manufacturer = source.readString();
        speed = source.readString();
        length = source.readString();
        passenger = source.readString();
        cargo = source.readString();
        crew = source.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLenght() {
        return length;
    }

    public String getCrew() {
        return crew;
    }

    public void setLenght(String lenght) {
        this.length = lenght;
    }

    public String getPasenger() {
        return passenger;
    }

    public void setPasenger(String pasenger) {
        this.passenger = pasenger;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeString(model);
        dest.writeString(clase);
        dest.writeString(manufacturer);
        dest.writeString(length);
        dest.writeString(speed);
        dest.writeString(passenger);
        dest.writeString(cargo);
        dest.writeString(crew);

    }
}
