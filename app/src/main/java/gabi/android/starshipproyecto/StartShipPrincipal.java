package gabi.android.starshipproyecto;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import java.util.List;
import gabi.android.starshipproyecto.rest.RetrofitClient;
import gabi.android.starshipproyecto.rest.StarshipApi;
import gabi.android.starshipproyecto.rest.StarshipService;
import gabi.android.starshipproyecto.rest.StarshipsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartShipPrincipal extends AppCompatActivity implements StarshipAdapter.OnClickListener{

        RecyclerView listStarships;

        List<StarshipApi> starshipApis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listStarships = findViewById(R.id.listStarships);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listStarships.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        listStarships.addItemDecoration(dividerItemDecoration);
       traerListaStarships();

    }


   public void traerListaStarships() {

        StarshipService starshipService = RetrofitClient.recuperarRetrofit().create(StarshipService.class);

        Call<StarshipsResponse> call = starshipService.getStarships();

        call.enqueue(new Callback<StarshipsResponse>() {
            @Override
            public void onResponse(Call<StarshipsResponse> call, Response<StarshipsResponse> response) {
                starshipApis = response.body().getStarships();
                mostrarListaStarships(starshipApis);
            }

            @Override
            public void onFailure(Call<StarshipsResponse> call, Throwable t) {

            }
        });
    }

    public void mostrarListaStarships(List<StarshipApi> starships) {

        StarshipAdapter starshipAdapter = new StarshipAdapter(this, starships,this);
        listStarships.setAdapter(starshipAdapter);
    }

    @Override
    public void click(int position) {
        Intent intent = new Intent(this, StarshipActivityDetalle.class);
        intent.putExtra("Nave",starshipApis.get(position));
        startActivity(intent);
    }
}