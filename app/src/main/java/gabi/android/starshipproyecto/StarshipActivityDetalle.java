package gabi.android.starshipproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import gabi.android.starshipproyecto.rest.StarshipApi;

public class StarshipActivityDetalle extends AppCompatActivity {

    private TextView starship_name_detalle;
    private TextView starship_model_detalle;
    private TextView starship_class_detalle;
    private TextView starship_manufacturer_detalle;
    private TextView starship_speed_detalle;
    private TextView starship_length_detalle;
    private TextView starship_passengers_detalle;
    private TextView starship_capacity_detalle;
    private  TextView starship_crew_detalle;
    private StarshipApi starshipApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        starshipApi = getIntent().getExtras().getParcelable("Nave");
        setContentView(R.layout.activity_starship_detalle);

        starship_name_detalle = findViewById(R.id.starship_name_detalle);
        starship_model_detalle = findViewById(R.id.starship_model_detalle);
        starship_class_detalle = findViewById(R.id.starship_class_detalle);
        starship_manufacturer_detalle = findViewById(R.id.starship_manufacturer_detalle);
        starship_speed_detalle = findViewById(R.id.starship_speed_detalle);
        starship_length_detalle = findViewById(R.id.starship_length_detalle);
        starship_passengers_detalle = findViewById(R.id.starship_passengers_detalle);
        starship_capacity_detalle = findViewById(R.id.starship_capacity_detalle);
        starship_crew_detalle = findViewById(R.id.starship_crew_detalle);

        starship_name_detalle.setText(starshipApi.getName());
        starship_model_detalle.setText(starshipApi.getModel());
        starship_class_detalle.setText(starshipApi.getClase());
        starship_manufacturer_detalle.setText(starshipApi.getManufacturer());
        starship_speed_detalle.setText(starshipApi.getSpeed());
        starship_length_detalle.setText(starshipApi.getLenght());
        starship_passengers_detalle.setText(starshipApi.getPasenger());
        starship_capacity_detalle.setText(starshipApi.getCargo());
        starship_capacity_detalle.setText(starshipApi.getCargo());
        starship_crew_detalle.setText(starshipApi.getCrew());

    }
}
